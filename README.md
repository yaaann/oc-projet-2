# oc-projet-3

Consigne du projet :

Concevez une carte interactive de location de vélos
____________________________________________________________________________________________________________________________________________________________________
TL ; DR

- Application simple page avec carte interactive pour réservation de vélos.

- Utilisation d'une API de cartographie et API JCDecaux pour les données de station en temps réel.

- développement Front seulement en JavaScript (+ HTML et CSS).

- Diaporama explicatif avec animation en JS.

- Utilisation Web Storage pour simuler la réservation.
____________________________________________________________________________________________________________________________________________________________________
Enoncé

Vous devez développer une page de type "Single page Application" simulant la réservation de vélos dans une ville. Ces vélos sont répartis dans de nombreuses stations dans la ville. L'utilisateur doit pouvoir réserver un vélo depuis son navigateur (à condition qu'il reste des vélos disponibles à la station !). La réservation est alors temporairement enregistrée sur le navigateur du visiteur.

Cette application doit notamment, en s'appuyant sur JavaScript, afficher une carte avec la liste des stations de location de vélos disponibles dans la ville.
Diaporama

Vous devez afficher en haut de la page un diaporama de photos et de textes expliquant le fonctionnement de l'application. La logique du diaporama doit être écrite par vos soins. L’utilisation de tout plugin automatisant la logique de l’application est proscrite.

Le diaporama passe automatiquement à la diaporama suivante toutes les 5 secondes. L’utilisateur peut toutefois choisir de mettre le diaporama en pause. Il peut également reculer ou avancer manuellement à l’aide d’un clic de souris, ainsi qu’avec les touches gauche et droite de son clavier.
Carte des vélos

En-­dessous du diaporama se trouve une carte affichant en temps réel la liste des stations de location de vélos ainsi que leur disponibilité. La localisation de toutes les stations de vélos est affichée à l’aide de marqueurs.

La localisation et l'état de chaque station (ouverte, en travaux, combien de vélos et de places sont disponibles, etc.) est fourni via la plateforme OpenData de JC Decaux.

Les données doivent provenir de l'API temps réel.

Un clic sur un marqueur affiche l’état de la station dans un panneau construit en HTML et CSS à côté de la carte.

La carte doit être générée dynamiquement via un service de cartographie.
Réservation d'un vélo

Il doit être possible de réserver un vélo disponible à la station sélectionnée en :

* indiquant son nom et son prénom,
* signant dans un champ libre implémenté à l’aide de l’API HTML5 Canvas.


Vous devez écrire vous même le code du Canvas. Aucun plugin n’est autorisé. Vous devez être capable d’expliquer votre code lors de la soutenance.

Une fois la réservation validée, un vélo est marqué comme réservé à cette station.

Pour ce projet, la réservation ne sera pas communiquée à un serveur. Seul le navigateur de l'utilisateur "retiendra" que le vélo a été réservé.

Les données de réservation seront stockées dans le navigateur à l’aide de l’API Web Storage et affichées en dessous du panneau. L'état de la réservation (s’il y en a une) est ainsi affiché, avec un décompte dynamique du temps restant avant expiration de la réservation.

Une réservation expire automatiquement au bout de 20 minutes et également lorsque le navigateur web se referme.

Le nom et le prénom sont toutefois conservés par le navigateur pour préremplir le formulaire de réservation lors d'un prochain usage, même si le navigateur a été fermé.

Il ne peut y avoir qu'une réservation à la fois. Si une nouvelle réservation a lieu, elle remplace la précédente.

Contraintes techniques

Le code JavaScript doit être conçu en Programmation Orientée Objet

Vous pouvez vous appuyer sur :

1. une librairie CSS telle que Bootstrap ou pure css,
2. une bibliothèque telle que jQuery pour manipuler le DOM.

Aucun plugin jQuery (ou autre) ne doit être utilisé pour la logique du diaporama.

Le code doit exploiter une API cartographique et l'API temps réel de API JCDecaux. Il doit également utiliser les API Web Storage et Canvas.
Référentiel d'évaluation
Créer des objets simples en JavaScript, contenant des méthodes et des propriétés

1. Le code JavaScript est développé en Orienté Objet
2. Le diaporama est conforme et fonctionnel
3. Aucun plugin n’est utilisé pour la logique de l’application (Diaporama, Canvas, Carte

Récupérer des données de formulaires en utilisant le langage JavaScript

1. Le canvas est fonctionnel
2. Le nom et prénom utilisent l’API LocalStorage
3. Les informations de réservations utilisent l’API SessionStorage
4. Les données de réservation sont affichées en dessous de la carte, s'il y a une réservation en cours

Faire des requêtes HTTP en langage JavaScript

1. La carte est récupérée dynamiquement depuis un web service cartographique
2. Les informations sur les stations utilisent l’API Live de JC Decaux


Écrire un code source lisible

1. Le code est correctement indenté
2. Les noms de classes, de méthode et de variables sont explicites (indifféremment en français ou en anglais)
3. Il y a une seule classe par fichier



