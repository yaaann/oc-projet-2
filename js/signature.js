class Signature {
    constructor() {
        this.width = 280,
        this.height = 100,
        this.signe = "no"
    };
    clear() {
        let canvas = document.getElementById("canvas");
        canvas.signe = "no";
        let ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, canvas.width, canvas.height);//Efface depuis origine jusqu'à canvas.dimensions
    };
    generation() {// première soumission du formulaire => affichage du canvas pour signature
        let buttonLine = document.getElementById("button-line");
        //Modification du formulaire existant
        formElt.removeEventListener("submit", formulaire.formSubmit);// suppression de la première soumission du formulaire
        formElt.removeChild(document.getElementById("nameLine"));//Suppression ligne renseignement du nom
        formElt.removeChild(document.getElementById("firstnameLine"));//Suppression ligne renseignement du nom
    
        // Création élément canvas
        //let canvasDiv = document.createElement("div");
        let canvasTextElt = document.createElement("div");// Le parent de <canvas> NE DOIT PAS avoir une position relative => création d'un <div> (relative) pour le <p> (absolute)
        canvasTextElt.id = "canvas-text";
        let canvasText = document.createElement("p"); //<p> (absolute)
        let canvasTextIcon = document.createElement("i");
        canvasTextIcon.className = "fas fa-pencil-alt";
        let canvasTextSpan = document.createElement("span");
        canvasTextSpan.textContent = " Signature";
        canvasText.appendChild(canvasTextIcon);
        canvasText.appendChild(canvasTextSpan);
        let canvas = document.createElement("canvas");
        canvas.id = "canvas";
        canvas.style.width = this.width + "px";
        canvas.style.height = this.height + "px";
        canvas.setAttribute("alt", "zone de signature");
        //evenement sur canvas
        canvas.addEventListener("mousedown", signature.beginDraw); 
        canvas.addEventListener("mouseup", signature.endDraw);
        canvas.addEventListener("mouseout", signature.endDraw); // Met fin au tracé si la souris sort de la zone
        canvas.addEventListener("touchstart", signature.beginDraw); 
        canvas.addEventListener("touchend", signature.endDraw);
    
        //Création des boutons supplémentaires
        let effacer = document.createElement("input");
        effacer.type = "button";
        effacer.id = "effacer";
        effacer.value = "Effacer";
        effacer.addEventListener("click", signature.clear);
        let retour = document.createElement("input");
        retour.type = "button";
        retour.id = "retour";
        retour.value = "Retour";
        retour.addEventListener("click", formulaire.affichageFormResa);
        
        //Insertion des nouveau éléments
        canvasTextElt.appendChild(canvasText);
        formElt.insertBefore(canvasTextElt, buttonLine);// insertion du canvas dans formulaire existant
        formElt.insertBefore(canvas, buttonLine);
        buttonLine.insertBefore(effacer, 
            document.getElementById("resa-submit"));// insertion boutons avant submit
        buttonLine.insertBefore(retour, effacer);// insertion boutons avant submit
        // Nouvel évênement soumission du formulaire
        formElt.addEventListener("submit", signature.controle);
    };
    draw(e) {
        let cible = e.currentTarget;
        let ctx = cible.getContext("2d")
        e.preventDefault();//Evite le scroll en tactile
        //nouvelles coordonnées relatives du pointeur sur canvas
        let pencil = signature.getPosition(e);
        ctx.lineWidth = 2;//Défini largeur ligne en px
        //Traçage ligne entre le point base et la nouvelle position relative du pointeur
        ctx.beginPath();
        ctx.moveTo(xBase, yBase);
        ctx.lineTo(pencil.posX, pencil.posY);
        ctx.stroke();
        // Position relative du pointeur devient base pour la prochaine ligne
        xBase = pencil.posX;
        yBase= pencil.posY;
    }
    getPosition(e) {
        let canvas = e.currentTarget;
        //Pour fonctionnement en tactile : 
        let eventEle = e.changedTouches? e.changedTouches[0]:e;
        //renvoie coordonnées relatives du pointeur sur canvas
        return {
            posX : (eventEle.pageX - canvas.offsetLeft) / (parseFloat(getComputedStyle(canvas).width) / 300),// la largeur de base de <canvas> = 300 px
            posY : (eventEle.pageY - canvas.offsetTop) / (parseFloat(getComputedStyle(canvas).height) / 150)// la hauteur de base de <canvas> = 150 px
        }
    }
    beginDraw(e) {
        let cible = e.currentTarget;
        e.preventDefault();//Evite le scroll en tactile
        let pencil = signature.getPosition(e);
        //Point de base prend les cooordonnées relatives du pointeur
        xBase = pencil.posX;
        yBase = pencil.posY;
        cible.addEventListener("mousemove", signature.draw);//Ajoute évênement au déplacement de la souris
        cible.addEventListener("touchmove", signature.draw);//Ajoute évênement au déplacement en tactile
        cible.signe = "yes";
    };
    endDraw(e) {
        let cible = e.currentTarget;
        cible.removeEventListener("mousemove", signature.draw);//Supprime évênement au déplacement de la souris
        cible.removeEventListener("touchmove", signature.draw);//Supprime évênement au déplacement en tactile
    };
    controle(e) {
        let canvas = document.getElementById("canvas");
        if (canvas.signe === "yes"){
            reservation.activation();
        } else {
            let signatureMessage = document.getElementById("signature-message");
            if (signatureMessage === null) {
                signatureMessage = document.createElement("div");
                let signatureMessageText = document.createElement("p");
                signatureMessageText.textContent = "Signez dans la zone ci-dessus !";
                signatureMessage.id = "signature-message";
                signatureMessage.appendChild(signatureMessageText);
                formElt.appendChild(signatureMessage);
            } 
        }
        e.preventDefault();
    }
}