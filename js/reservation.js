class Reservation {
    constructor(customer) {
    this.customer = customer,
    this.station =  null;
    this.time = null;
    }
    etat() {
        //reservation = JSON.parse(localStorage.getItem("reservation"));
        this.station = JSON.parse(sessionStorage.getItem("station"));
        this.customer = JSON.parse(localStorage.getItem("customer"));
        if ((this.station !== null) && (this.customer !== undefined)){
            errorResa = 0;
            return `Vélo réservé à la station ${renameStation(this.station.name)} par ${this.customer.firstname} ${this.customer.name}`;
        } else {
            errorResa = 1;
            return `Désolé, une erreur s'est produite :/ merci de renouveler votre demande.`
        }
    };
    affichage() {
        let etatElt = document.getElementById("etat-resa");
        etatElt.innerHTML = "";
        //Ligne avec nom client et station
        let reservationEtat = document.createElement("p");
        reservationEtat.textContent = reservation.etat();
        if (errorResa === 0 ) {
            etatElt.appendChild(reservationEtat);
            //Ligne avec temps restant
            etatElt.insertAdjacentHTML("beforeend",
                "<p>Temps restant : <span id='minutes'>0</span> min <span id='secondes'>0</span>s</p>");
            // Affichage compteur et lancement
            reservation.timer();
            let interval = setInterval(reservation.timer, 1000)// compteur s'actualise toutes les secondes
            // Bouton d'annulation de la réservation
            let boutonAnnule = document.createElement("input");
            boutonAnnule.type = "button";
            boutonAnnule.value = "Annuler réservation";
            boutonAnnule.id = "annulation";
            boutonAnnule.addEventListener("click", function() {
                if (window.confirm("Voulez-vous vraiment annuler cette réservation")) {// demande de confirmation avant annulation
                reservation.delete();
                }
            }); 
            etatElt.appendChild(boutonAnnule);
        }
    };
    storageLocal() {
        localStorage.setItem("reservation", JSON.stringify(reservation));
    };
    storageSession() {
        sessionStorage.setItem("station", JSON.stringify(this.station));
        sessionStorage.setItem("time", JSON.stringify(this.time));
    };
    storageCustomer() {
        localStorage.setItem("customer", JSON.stringify(this.customer));
    };
    activation() {
        ajaxGet(urlStations, function (reponse){// ultime requête pour vérifier qu'il reste des vélos
            let stations = JSON.parse(reponse);
            stations.forEach(station => {
                if (station.number === reservation.station.number) {//Recherche station par numéro
                    if (station.totalStands.availabilities.bikes > 0) {// on s'assure qu'il y a toujours des vélos dispos
                        reservation.station.totalStands.availabilities.bikes -= 1;//diminue quantité de 1
                        let dispoStationText = document.getElementById("compteur_velos_dispos");
                        dispoStationText.textContent = reservation.station.totalStands.availabilities.bikes + " Vélos disponibles";
                        reservation.time = Date.now();
                        //reservation.storageLocal();//stockage de l'objet réservation dans localStorage
                        reservation.storageSession();//Stockage station et date dans sessionStorage
                        reservation.storageCustomer();//stockage de l'objet client dans localStorage
                        reservation.affichage();
                        formElt.innerHTML = "";//Efface formulaire
                        window.scrollTo(0, window.innerHeight);//scrolle en bas de page pour que la zone d'état soit visible.
                        generationMarker(reponse);
                    } else {noBikes()}
                }
            })
        });
    };
    delete() {
        // Suppression station et date de réservation
        reservation.station = null;
        reservation.time = null;
        //"Enregistrement" de la suppression dans le localStorage
        //localStorage.setItem("reservation", JSON.stringify(reservation)); //A activer dans le cas où la réservation est enregistrée dans le local storage.
        //"Enregistrement" de la suppression dans le sessionStorage
        sessionStorage.setItem("station", JSON.stringify(this.station)); //A desactiver dans le cas où la réservation est enregistrée dans le local storage.
        sessionStorage.setItem("time", JSON.stringify(this.time)); //A desactiver dans le cas où la réservation est enregistrée dans le local storage.
    };
    timer(){
        let timeRef = Date.now();//Date en temps réel (millisecondes)
        let minutesLeft = document.getElementById("minutes");
        let secondesLeft = document.getElementById("secondes");
        let timeLeft = deltaTimeMax - (timeRef - reservation.time);// calcul temps restant en temps réel
        if (timeLeft > 0) { // Tant qu'il reste du temps
            let deltaDate = new Date(timeLeft);//transforme en date au format jjj mmm, etc
            minutesLeft.textContent = deltaDate.getMinutes();// récupération minutes du temps restant
            secondesLeft.textContent = deltaDate.getSeconds();// récupération secondes du temps restant
        } else {
            reservation.delete();
            location.reload();
        }
    }
}

