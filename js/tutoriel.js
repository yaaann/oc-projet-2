let diaporama = new Diaporama()
diaporama.element = document.getElementById("diaporama");
//let diapositives = []//tableau qui contiendra les diapos
let diapositivesElts = document.getElementsByClassName("diapositive");//Sélection des éléments HTML

let diapoIndex = 0;// index de la diapo qui doit s'afficher (0 de base)
let position = 0;// variable de la position de chaque diapo
let diapoRef = (document.getElementById("tuto-0"));//Diapo de ref. (on prend la première)
// On détermine la largeur des diapositives
let diapoWidth = parseFloat(getComputedStyle(diapoRef).width) + parseFloat(getComputedStyle(diapoRef).paddingLeft) + parseFloat(getComputedStyle(diapoRef).paddingRight) + parseFloat(getComputedStyle(diapoRef).borderLeftWidth) + parseFloat(getComputedStyle(diapoRef).borderRightWidth);
//Ajout des éléments HTML au tableau des diapos en ajoutant les paramètres de la classe
for (let i = 0; i < diapositivesElts.length; i++) {
    diaporama.diapositives.push(new Diapositive(diapositivesElts[i]))
};

// calcul ecartement entre les diapositives.
let ecartDiapo = (parseFloat(getComputedStyle(diaporama.element).width) - diapoWidth) / (diaporama.diapositives.length - 1);
let baseScroll;
let intervalLecture;

let beginButton = document.getElementById("begin");
let backButton = document.getElementById("back");
let pauseButton = document.getElementById("pause");
let playButton = document.getElementById("play");
let nextButton = document.getElementById("next");
let endButton = document.getElementById("end");

//Initialisation du compteur du diaporama
diaporama.compteur();
//Initialisation diaporama au chargement de la page
diaporama.initialisation();
