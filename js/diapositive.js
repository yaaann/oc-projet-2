//Classe pour création des objet "diapositive"
class Diapositive {
    constructor(element) {
      this.element = element;
      this.angle = 0;
      this.origine = "";
      this.positionLeft = "";
      this.positionRight = "";
      this.zIndex = 0
    }
    decrire() {
      return `${this.element} a  un angle de ${this.angle} degrés depuis
       ${this.origine} et est situé à ${this.positionLeft} du bord gauche et
       ${this.positionRight} du bord droit`;
    }
    toLeft() {
        this.angle = 90;
        this.origine = "top left";
        this.positionLeft = position + "px" ;
        this.positionRight = "auto";
        this.zIndex = diaporama.diapositives.indexOf(this);
    }
    toCenter() {
        this.angle = 0;
        this.origine = "";
        this.positionLeft = position + "px" ;
        this.positionRight = "auto";
        this.zIndex = diaporama.diapositives.length;
    }
    toRight() {
        this.angle = -90;
        this.origine = "top right";
        this.positionLeft = "auto";
        this.positionRight = parseFloat(getComputedStyle(diaporama.element).width) - position + "px" ;
        this.zIndex = diaporama.diapositives.length - diaporama.diapositives.indexOf(this);
    }
    intitialisation() {
        this.element.style.margin = "5px 0";
        this.element.style.transformOrigin = this.origine;
        this.element.style.transform = "rotateY("+ this.angle + "deg)";
        this.element.style.zIndex = this.zIndex;
        this.element.style.position = "absolute";
        this.element.style.left = this.positionLeft;
        this.element.style.right = this.positionRight;
        this.element.style.boxShadow = "5px 0px 0px #303030, -5px 0px 0px #303030";
    }
    
}
