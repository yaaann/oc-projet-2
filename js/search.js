//API Nominatim
const urlSearch = "https://nominatim.openstreetmap.org/search?";//cf. https://nominatim.org/release-docs/develop/api/Search/
const pays = "&countrycode=fr";// limite les résultats en France
const searchArea = "&viewbox=47.2591,-1.6238,47.1696,-1.5062"
const format = "&format=json";// Format de la réponse
const limite = "&limit=1";//Nombre de résultat
const email = "&email=yann.tachier@gmail.com";//Adresse mail valide
const parametres = pays + searchArea + format + limite + email;

//Layer pour le marqueur de résultat
let resultatLayer = L.layerGroup();

let searchForm = document.getElementById("search-form");

//La définition des marqueurs
let markIconBig = L.Icon.extend({
    options: {
        shadowUrl: "assets/marker-shadow.png",
        iconSize: [44, 70],
        shadowSize: [70, 70],
        iconAnchor: [22, 70],
        shadowAnchor: [22, 70],
        popupAnchor: [0, -60]
    }
});

let redMarkIconBig = new markIconBig({ iconUrl: "assets/map-marker-red.png" });

function nominatumSearch(e) {
    let query = document.getElementById("query").value;
    if (query.length > 3) {
        ajaxGet(urlSearch + "q=" + query + parametres, markLocation)
    } else{
        noResults();
    }
    e.preventDefault();
}

function noResults() {
    let noResMessage = document.createElement("p");
    noResMessage.id = "no-results"
    noResMessage.textContent = "Pas de résultat";
    searchForm.appendChild(noResMessage);
}

function toLocation(e) {
    let marker = e.target;
    let positionMarker = marker.getLatLng();
    map.panTo(positionMarker);
}

function zoomLocation(e) {
    let marker = e.target;
    let positionMarker = marker.getLatLng();
    map.flyTo(positionMarker, 17)
}

function markLocation(reponse) {
    let resultat = JSON.parse(reponse);
    if (resultat.length === 0) {
        noResults();
    }else {
        let noResMessage = document.getElementById("no-results");
        if (noResMessage !== null) {// s'il a eu un message d'afficher avant => suppression
            searchForm.removeChild(noResMessage);
        }
        resultatLayer.clearLayers();
        L.marker([resultat[0].lat, resultat[0].lon]).addTo(resultatLayer)
            .setIcon(redMarkIconBig)
            .setZIndexOffset(10)
            .on({
                click: toLocation,
                dblclick: zoomLocation 
            });
        resultatLayer.addTo(map);
        map.flyTo([resultat[0].lat, resultat[0].lon], 16.5)
    }
}

searchForm.addEventListener("submit", nominatumSearch);
