class Formulaire {
    noBikes() {
        let nobikesMessage = document.createElement("div");
        let nobikesMessageText = document.createElement("p");
        nobikesMessageText.textContent = "Désolé, plus de vélos disponibles. Essayez une autre station.";
        nobikesMessage.id = "nobikes-messages";
        nobikesMessage.appendChild(nobikesMessageText);
        infosElt.appendChild(nobikesMessage);
    };
    affichageDispo(e) {// Vélo et places disponibles
        let libreStation = document.createElement("p");
        libreStation.id = "places-libres";
        let libreStationText = document.createElement("span");
        libreStationText.textContent = e.totalStands.availabilities.stands + " Places disponibles";
        let dispoStation = document.createElement("p");
        dispoStation.id = "velos-dispos";
        let dispoStationText = document.createElement("span")
        dispoStationText.textContent = e.totalStands.availabilities.bikes + " Vélos disponibles";
        dispoStationText.id = "compteur_velos_dispos";
        let iconeVelo = document.createElement("i");
        iconeVelo.className = "fas fa-bicycle";
        let iconePlace = document.createElement("i");
        iconePlace.className = "fas fa-parking";
        libreStation.appendChild(iconePlace);
        libreStation.appendChild(libreStationText);
        dispoStation.appendChild(iconeVelo);
        dispoStation.appendChild(dispoStationText);
        infosElt.appendChild(dispoStation);
        infosElt.appendChild(libreStation);
    };
    formSubmit(e) {
        if (reservation.station.totalStands.availabilities.bikes > 0) {//Deuxième vérif s'il reste des vélos
            reservation.customer.name = formElt.elements.yourname.value.toUpperCase();
            reservation.customer.firstname = formElt.elements.yourfirstname.value.toUpperCase();
            signature.generation();
        } else {
            this.noBikes();
        }
        e.preventDefault();
    };
    affichageFormResa() {// Formulaire de réservations
        formElt.innerHTML = "";//Efface ancien formulaire
        formElt.removeEventListener("submit", signature.controle);
        let nameInputLabel = document.createElement("label");
        nameInputLabel.setAttribute("for", "name-input");
        nameInputLabel.textContent = "Nom : ";
        let firstnameInputLabel = document.createElement("label");
        firstnameInputLabel.setAttribute("for", "firstname-input");
        firstnameInputLabel.textContent = "Prénom : ";
        let nameInput = document.createElement("input");
        nameInput.id = "name-input";
        nameInput.name = "yourname";
        nameInput.setAttribute("required", "true");
        let firstnameInput = document.createElement("input");
        firstnameInput.id = "firstname-input";
        firstnameInput.name = "yourfirstname";
        firstnameInput.setAttribute("required", "true");
        if (reservation.customer !== null) {// pré-rempli nom & prénom si client existe 
            nameInput.value = reservation.customer.name;
            firstnameInput.value = reservation.customer.firstname;
        }
        let nameElt = document.createElement("p");
        nameElt.id = "nameLine";
        let firstnameElt = document.createElement("p");
        firstnameElt.id = "firstnameLine";
        let buttonLine = document.createElement("p");
        buttonLine.id = "button-line";
        let reservationSubmit = document.createElement("input");
        reservationSubmit.type = "submit";
        reservationSubmit.value = "Réserver";
        reservationSubmit.id = "resa-submit";
        formElt.addEventListener("submit", formulaire.formSubmit);// Soumission du formulaire au serveur
        nameElt.appendChild(nameInputLabel);
        nameElt.appendChild(nameInput);
        firstnameElt.appendChild(firstnameInputLabel);
        firstnameElt.appendChild(firstnameInput);
        formElt.appendChild(nameElt);
        formElt.appendChild(firstnameElt);
        buttonLine.appendChild(reservationSubmit);
        formElt.appendChild(buttonLine);
    };
    affichageInfoStation(e) {
        infosElt.innerHTML = "";
        let nomStation = document.createElement("h3");
        nomStation.textContent = renameStation(e.name);
        let adresseElt = document.createElement("p");
        adresseElt.id = "adresse";
        let adresseStation = document.createElement("span");
        adresseStation.textContent = e.address;
        infosElt.appendChild(nomStation);
        infosElt.appendChild(adresseElt);
        adresseElt.insertAdjacentHTML("afterbegin",
            "<strong>Adresse :</strong><br/>");
        adresseElt.appendChild(adresseStation);
        if (e.status.toUpperCase() === "OPEN") {//Si la station est ouverte
            this.affichageDispo(e);// Vélo et places disponibles
            if (reservation.station.totalStands.availabilities.bikes > 0) {//Vérif s'il reste des vélos
                this.affichageFormResa(); // Formulaire de réservations
            } else {
                formElt.innerHTML = "";
                this.noBikes();
            };
        } else {// si la station est fermée ou autre
            formElt.innerHTML = "";
            adresseElt.insertAdjacentHTML("beforeend",
                "<div id='closed'><p>La station est actuellement fermée.</p></div>");
        }
    };
}