class Diaporama {
    constructor() {
        this.element = null;
        this.diapositives = [];
    }
    lecture() {
        if (diapoIndex < diaporama.diapositives.length-1) {
            diaporama.next()
        } else {
            diaporama.begin();
        }
    }
    repriseLecture () {
        setTimeout(diaporama.lecture, 2000);
        intervalLecture = setInterval(diaporama.lecture, 5000);
        playButton.style.color = "green";
        pauseButton.style.color = "black";
    }
    pause () {
        clearInterval(intervalLecture);
        playButton.style.color = "black";
        pauseButton.style.color = "green";
    }
    begin () {
        for (let i = diapoIndex; i > 0; i--) {
            diaporama.back();
        }
    }
    back () {
        let currentDiapo = diaporama.diapositives[diapoIndex];
        let previousDiapo = diaporama.diapositives[diapoIndex-1];
        if (diapoIndex > 0) {
            position = parseFloat(currentDiapo.positionLeft) + diapoWidth;
            currentDiapo.toRight();
            position = parseFloat(previousDiapo.positionLeft);
            previousDiapo.toCenter();
            currentDiapo.intitialisation();
            previousDiapo.intitialisation();
            diapoIndex -=1;
            diaporama.compteur();
        }
    }
    next () {
        let currentDiapo = diaporama.diapositives[diapoIndex];
        let nextDiapo = diaporama.diapositives[diapoIndex+1];
        if (diapoIndex < diaporama.diapositives.length-1) {
            position = parseFloat(currentDiapo.positionLeft);
            currentDiapo.toLeft()
            position = position + ecartDiapo;
            nextDiapo.toCenter();
            currentDiapo.intitialisation();
            nextDiapo.intitialisation();
            diapoIndex +=1;
            diaporama.compteur();
        } 
    }
    end () {
        for (let i = diapoIndex; i < diaporama.diapositives.length; i++) {
            diaporama.next();
        }
    }
    defilClavier(e) {
        switch (e.keyCode) {
            case 37:
                diaporama.back();
                break;
            case 39:
                diaporama.next()
                break;
        }
    }
    scrollRight() {
        if (diapoIndex < diaporama.diapositives.length-1) {
            let scrollX = parseFloat(getComputedStyle(diaporama.element).width) / diaporama.diapositives.length;        
            diaporama.element.style.transform = "translateX(-" + scrollX*(diapoIndex+1) + "px)"
            diapoIndex +=1;
            diaporama.compteur();
        }
    }
    scrollLeft() {
        if (diapoIndex > 0) {
            let scrollX = parseFloat(getComputedStyle(diaporama.element).width) / diaporama.diapositives.length;        
            diaporama.element.style.transform = "translateX(-" + scrollX*(diapoIndex-1) + "px)"
            diapoIndex -=1;
            diaporama.compteur();
        }
    }
    compteur() {
        let compteurDiapo = document.getElementById("compteur-diapo");
        compteurDiapo.textContent = diapoIndex + 1 + " / " + diaporama.diapositives.length;
    }
    getPosition(e) {
        let eventEle = e.changedTouches? e.changedTouches[0]:e;
        return {
            posX : eventEle.pageX
        }
    }
    beginTactilScroll(e) {
        let cible = e.currentTarget;
        e.preventDefault();
        let finger = diaporama.getPosition(e);
        baseScroll = finger.posX;
        cible.addEventListener("touchmove", diaporama.tactilScroll);
    }
    tactilScroll(e) {
        let cible = e.currentTarget;
        e.preventDefault();
        let finger = diaporama.getPosition(e);
        let newScroll = finger.posX;
        if (window.innerWidth >= 1024) {
            if (baseScroll > newScroll) {
                diaporama.next();
            } else if (baseScroll < newScroll) {
                diaporama.back();
            }
        } else if (baseScroll > newScroll) {
            diaporama.scrollRight();
        } else if (baseScroll < newScroll) {
            diaporama.scrollLeft();
        }
        cible.removeEventListener("touchmove", diaporama.tactilScroll);
    }
    initialisation() {
        //Mise en place du diaporama sur écrans moyens et larges, sinon lien vers autre page (cf.css)
        if (window.innerWidth >= 1024) {
            //Ajout évènements pour défilement du diaporama
            beginButton.addEventListener("click", diaporama.begin);
            backButton.addEventListener("click", diaporama.back);
            pauseButton.addEventListener("click", diaporama.pause);
            playButton.addEventListener("click", diaporama.repriseLecture);
            nextButton.addEventListener("click", diaporama.next);
            endButton.addEventListener("click", diaporama.end);
            window.addEventListener("keydown", diaporama.defilClavier);
            //parcours le tableau des diapos
            diaporama.diapositives.forEach(diapositive => {
                if (diaporama.diapositives.indexOf(diapositive) < diapoIndex) {
                    diapositive.toLeft();
                } else if (diaporama.diapositives.indexOf(diapositive) === diapoIndex) {
                    diapositive.toCenter();
                    position = position + diapoWidth;
                } else if (diaporama.diapositives.indexOf(diapositive) > diapoIndex) {
                    diapositive.toRight();
                }
                diapositive.element.addEventListener("touchstart", diaporama.beginTactilScroll);
                diapositive.intitialisation();
                position = position + ecartDiapo;
            })
            //Lancement de la lecture et "activation du bouton"
            intervalLecture = setInterval(diaporama.lecture, 5000);
            playButton.style.color = "green";
        } else {
            //Nouvelle mise en forme diaporama
            diaporama.element.style.width = 100 * diaporama.diapositives.length + "%";
            diaporama.diapositives.forEach(diapositive => {
                diapositive.element.style.maxWidth = screen.width - 80 + "px";
                diapositive.element.style.maxHeight = screen.height * 0.8 + "px"
                diapositive.element.style.marginRight = "10px";
                diapositive.element.style.marginLeft = "10px";
                diapositive.element.addEventListener("touchstart", diaporama.beginTactilScroll);
            });
            //Création bouton pour afficher le diaporama
            let tutoButton = document.getElementById("tuto-mobile");
            let commandesDiapo = document.getElementById("commandes-diapo")
            let closeDiapo = document.getElementById("close-diapo");
            tutoButton.addEventListener("click", function() {
                diaporama.element.style.display = "flex";
                commandesDiapo.style.display = "flex";
                closeDiapo.style.display = "flex"
                backButton.addEventListener("click", diaporama.scrollLeft);
                nextButton.addEventListener("click", diaporama.scrollRight);
                closeDiapo.addEventListener("click", function() {
                    diaporama.element.style.display = "none";
                    commandesDiapo.style.display = "none";
                    closeDiapo.style.display = "none"
                })
            });
        }
    }
}