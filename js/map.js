//Commande du bouton pour vider le localStorage (recharge la page)
document.getElementById("delete").addEventListener("click", function() {
    localStorage.clear();
    location.reload();
})

//Pour les reqûetes AJAX chez JCDecaux
const ville = "nantes" //Nom du contrat chez JCDecaux
const APIKey = "b5c6b87bcfa0082a4ca14872a2e3d82451f2eb6a" // Fournie par JCDecaux
const urlStations = "https://api.jcdecaux.com/vls/v3/stations?contract=" + ville + "&apiKey=" + APIKey;

let infosElt = document.getElementById("informations");
let formElt = document.getElementById("form-reservation");
let popupHover;//Popup qui s'affichera au survol d'une station
let errorResa;// variable qui transmet état d'erreur entre fonctions

let markers = [];//tableau des marqueurs de carte
let markerLayer = L.layerGroup(markers); //calque avec les marqueurs de carte

//La définition des marqueurs
let markIcon = L.Icon.extend({
    options: {
        shadowUrl: "assets/marker-shadow.png",
        iconSize: [35, 41],
        shadowSize: [41, 41],
        iconAnchor: [17, 41],
        shadowAnchor: [13, 41],
        popupAnchor: [0, -37]
    }
});

let greenMarkIcon = new markIcon({ iconUrl: "assets/marker-icon-green.png" });
let redMarkIcon = new markIcon({ iconUrl: "assets/marker-icon-red.png" });
let greyMarkIcon = new markIcon({ iconUrl: "assets/marker-icon-grey.png" });
let whiteMarkIcon = new markIcon({ iconUrl: "assets/marker-icon-white.png" });
let blueMarkIcon = new markIcon({ iconUrl: "assets/marker-icon-blue.png" });

//Point de base pour le tracé de le signature dans le <canvas>
let xBase;
let yBase;

//L'objet avec toutes les données à conserver
/* NOTA : 
Le client est conservé dans le localStorage, La réservation est conservée dans sessionStorage.
L'application peut fonctionner en conservant une réservation en localStorage.
Modifications à faire : 
    > Fichier actuel :
        - Désactiver L 74 à 79 & Activer L 80 et 81,
    > Fichier reservation.js :
        - Activer L 8 & désactiver L 9-10, dans etat()
        - Activer L 61 & désactiver L 62, dans activation()
        - Activer L 78 & désactiver L 80-81, dans delete()

        
*/
let client = {
    name : "",
    firstname : "",
    signature : null
};

let reservation = new Reservation(client);
let formulaire = new Formulaire();
let signature = new Signature();//zone de signature avec dimensions

//La date au chargement de la page comme référence
let timeInitial = Date.now(); // en millisecondes
// Construit date au format jjj mmm, etc, à partir de la valeur en millisecondes
let dateIntiale = new Date(timeInitial);
//Délai max de conservation 20 minutes soit 1.200.000 millisecondes
let deltaTimeMax = 1200000;

// si une réservation existe dans local storage = assignation des données à la réservation courante
if (JSON.parse(localStorage.getItem("customer")) !== null) {//A désactiver si localStorage 
    reservation.customer = JSON.parse(localStorage.getItem("customer"));//A désactiver si localStorage
    if (JSON.parse(sessionStorage.getItem("station")) !==null) {//A désactiver si localStorage
        reservation.station = JSON.parse(sessionStorage.getItem("station"));
        reservation.time = JSON.parse(sessionStorage.getItem("time"));
    };
//if (JSON.parse(localStorage.getItem("reservation")) !== null) {//A activer si localStorage 
    //reservation = JSON.parse(localStorage.getItem("reservation"));//A activer si localStorage 
    let timeLeft = deltaTimeMax - (timeInitial - reservation.time);//calcul temps restant
    if (timeLeft < 0){// si la réservation a plus de 20 minutes => supprimée
        reservation.delete();
    } else { 
        reservation.affichage();
    }   
}

function renameStation(nomStation) {//Pour supprimer la chaîne de type "#00000-" au début de tous les noms
    let indexTiret = nomStation.indexOf("-");
    return nomStation.slice(indexTiret+1);
}

function popupStationOff() {//Fonction qui masque le popup
    popupHover.removeFrom(map);
};

function popupStationOn(e) {//Fonction qui affiche le popup
    let marker = e.target;//La cible est le marqueur
    let positionMarker = marker.getLatLng();//Récupération des coordonnées de la cible
    let popupDef = marker.getPopup();//Récupération du popup de la cible
    popupHover = L.popup({ offset: [0, -30] })//Définition du popup à afficher [décalage du point origine]
        .setLatLng(positionMarker)//Coordonnées
        .setContent(popupDef.getContent())//Contenu
        .openOn(map);
};

function generationMarker(reponse) {
    let stations = JSON.parse(reponse);//conversion en objet JS
    let icone;//variable icone
    markerLayer.clearLayers();//Vidage du calque des marqueurs
    markers = [];// Vidage tableau contenant les marqueurs
    stations.forEach(station => {
        //Définition de la couleur du marqueur
        if (station.status.toUpperCase() === "CLOSED") {
            icone = greyMarkIcon;
        } else if ((reservation.station !== null ) && (station.number === reservation.station.number )) {
            icone = redMarkIcon;
            //S'il s'agit de la station sur laquelle il y a déjà une réservation, on prends les données locales pour le compteur de vélos dispos
            station = reservation.station; 
        } else if (station.totalStands.availabilities.bikes > 0 ) {
            icone = greenMarkIcon;
        } else if (station.totalStands.availabilities.stands === 0) {
            icone = blueMarkIcon;
        } else {
            icone = whiteMarkIcon;
        }
        //Définition du marqueur et ajout au tableau puis au calque
        markers.push(L.marker([station.position.latitude, station.position.longitude], { icon: icone }).addTo(markerLayer)
            .bindPopup(function () { // Contenu du popup
                let popupContent = document.createElement("div");
                let popupName = document.createElement("p");
                popupName.className = "popup_nom";
                popupName.textContent = renameStation(station.name);
                let popupDispo = document.createElement("p");
                popupDispo.className = "popup-dispo"
                let popupDispoVelo = document.createElement("span");
                let popupDispoVeloText = document.createElement("span");
                popupDispoVeloText.textContent = station.totalStands.availabilities.bikes;
                let popupDispoVeloIcon = document.createElement("i");
                popupDispoVeloIcon.className = "fas fa-bicycle";
                let popupDispoPlace = document.createElement("span");
                let popupDispoPlaceText = document.createElement("span");
                popupDispoPlaceText.textContent = station.totalStands.availabilities.stands;
                let popupDispoPlaceIcon = document.createElement("i");
                popupDispoPlaceIcon.className = "fas fa-parking";
                popupDispoVelo.appendChild(popupDispoVeloIcon);
                popupDispoVelo.appendChild(popupDispoVeloText);
                popupDispo.appendChild(popupDispoVelo);
                popupDispoPlace.appendChild(popupDispoPlaceIcon);
                popupDispoPlace.appendChild(popupDispoPlaceText);
                popupDispo.appendChild(popupDispoPlace);
                popupContent.appendChild(popupName);
                popupContent.appendChild(popupDispo);
                return popupContent;
            })
            .on({
                click: function () {
                    reservation.station = station;
                    formulaire.affichageInfoStation(reservation.station);
                },
                mouseover: popupStationOn,//Apparition du popu au survol
                mouseout: popupStationOff//Enlève popup
            }));
    });
    markerLayer.addTo(map);//Ajout du calque à la carte
};

//Définition et insertion de la carte
const map = L.map('map').setView([47.2198, -1.5546], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

//requête vers JCDecaux pour la liste des stations et génération des marqueurs de carte
ajaxGet(urlStations, generationMarker);

//La requete des parkings ne fonctionne pas avec cette cle API (403 Forbidden). Mais fonctionne avec une cle copiee sur site officiel...

/*ajaxGet("https://api.jcdecaux.com/parking/v1/contracts/" + ville + "/parks?apiKey=" + APIKey + "&contract=" + ville, function (reponse) {
    parkings = JSON.parse(reponse);
    parkings.forEach(parking => {
        L.marker([parking.position.latitude, parking.position.longitude]).addTo(map)
            .bindPopup(parking.name);
    });
});
*/
//https://api.jcdecaux.com/parking/v1/contracts/nantes/parks?apiKey=b5c6b87bcfa0082a4ca14872a2e3d82451f2eb6a&contract=nantes


